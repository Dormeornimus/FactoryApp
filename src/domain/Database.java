/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Dorme Ornimus
 */
public class Database{
    private static Database uniqueInstance;
    private static List<Individual> data;
    
    private Database(){
            
        data = new ArrayList<>();
        BufferedReader in = null;
        try {
            File file = new File("src\\Data\\database.txt");
            in = new BufferedReader(new FileReader(file));
            String line;
            while((line = in.readLine()) != null)
            {
                if(line.matches("[0-9]{8}\\|[a-zA-Z]*\\|[a-zA-Z]*\\|[a-zA-Z]*\\|[0-9]{1,3}\\|(M|F)\\|[a-zA-Z]*\\|[0-9]{1,2}")){
                    String[] aux = line.split("\\|");
                    data.add(new Individual(aux[0],aux[1],aux[2],aux[3],aux[4],aux[5],aux[6],aux[7]));
                    
                }
            }
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static Database getInstance(){
        if(uniqueInstance == null){
            uniqueInstance = new Database();
        }
        return uniqueInstance;
    }
    
    public List<Individual> getData(){
        return data;
    }
}
