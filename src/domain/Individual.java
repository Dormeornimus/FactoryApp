/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author Dorme Ornimus
 */
public class Individual {
    private String controlnumber;
    private String name;
    private String surname;
    private String lastname;
    private String age;
    private String sex;
    private String career;
    private String semester;
    
    public Individual(String controlnumber,String name,String surname, String lastname,String age, String sex, String career, String semester){
            this.controlnumber=controlnumber;
            this.name=name;
            this.surname=surname;
            this.lastname=lastname;
            this.age=age;
            this.sex=sex;
            this.career=career;
            this.semester=semester;
            }
    
    public String getControlNumber(){
        return controlnumber;
    }
    public String getName(){
        return name;
    }
    public String getSurname(){
        return surname;
    }
    public String getLastName(){
        return lastname;
    }
    public String getAge(){
        return age;
    }
    public String getSex(){
        return sex;
    }
    public String getCareer(){
        return career;
    }
    public String getSemester(){
        return semester;
    }   
    public String toString(){
        return name;
    }
}
