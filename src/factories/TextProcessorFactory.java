/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factories;

import products.Csv;
import products.Fileable;
import products.Json;
import products.Txt;
import products.Xml;

/**
 *
 * @author Dorme Ornimus
 */
public class TextProcessorFactory {
   
    public Fileable create(String type){
        if(type.matches("txt")){
            return new Txt();
        }
        if(type.matches("xml")){
            return new Xml();
        }
        if(type.matches("json")){
            return new Json();
        }
        if(type.matches("csv")){
            return new Csv();
        }
        return null;
    }
}
