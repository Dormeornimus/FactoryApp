/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryapp;

import domain.Database;
import products.Csv;
import products.Json;
import products.Txt;
import products.Xml;

/**
 *
 * @author Dorme Ornimus
 */
public class FactoryApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Txt txt = new Txt();
        txt.createfile();
        Csv csv = new Csv();
        csv.createfile();
        Xml xml = new Xml();
        xml.createfile();
        Json json = new Json();
        json.createfile();
        
    }
    
}
