/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

import java.io.FileWriter;

import domain.Database;
import domain.Individual;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Dorme Ornimus
 */
public class Json implements Fileable{

    public Json(){
        
    }
    @Override
    public void createfile() {
	try {
            int c = 0;
            JSONObject document = new JSONObject();
            JSONArray arr2 = new JSONArray();
            for(Individual o :Database.getInstance().getData()){
                JSONArray arr = new JSONArray();
                arr.add("Numero de Control: "+o.getControlNumber());
                arr.add("Nombre: "+ o.getName());
                arr.add("Apellido Materno: "+o.getSurname());
                arr.add("Apellido Paterno: "+o.getLastName());
                arr.add("Edad: "+o.getAge());
                arr.add("Sexo: "+o.getSex());
                arr.add("Carrera: "+o.getCareer());
                arr.add("Semestre: "+o.getSemester());
                arr2.add("ID: "+Integer.toString(c)+" Information: "+arr+"\n");
                c++;
                }
                document.put("Alumnado",arr2);
                FileWriter file = new FileWriter("src\\Data\\data.json");
                file.write(document.toJSONString());
        } catch (IOException ex) {
            Logger.getLogger(Json.class.getName()).log(Level.SEVERE, null, ex);
        }
                   
        
    }
    
}
