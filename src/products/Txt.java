/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

import domain.Database;
import domain.Individual;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dorme Ornimus
 */
public class Txt implements Fileable {


    @Override
    public void createfile() {
        String aux="";
        for (Individual o :Database.getInstance().getData()){
            aux += o.getControlNumber()+"\t";
            aux += o.getName()+"\t";
            aux += o.getSurname()+"\t";
            aux += o.getLastName()+"\t";
            aux += o.getAge()+"\t";
            aux += o.getSex()+"\t";
            aux += o.getCareer()+"\t";
            aux += o.getSemester()+"\n";
        }
        try (FileWriter file = new FileWriter("src\\Data\\data.txt")) {
            file.write(aux);
                            
        } catch (IOException ex) {
            Logger.getLogger(Txt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
