/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

import domain.Database;
import domain.Individual;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 *
 * @author Dorme Ornimus
 */
public class Xml implements Fileable{
    
    public Xml(){
        
    }

    @Override
    public void createfile() {
        
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            List<Individual> data = Database.getInstance().getData();
            int c= 0;
            Element individuals = doc.createElement("Individuals");
                doc.appendChild(individuals);
            
            for(Individual o : data){
                
                
		Element element = doc.createElement("Individual");
		individuals.appendChild(element);
                
		Attr attr = doc.createAttribute("id");
		attr.setValue(Integer.toString(c));
		element.setAttributeNode(attr);

                
                Element controlnumber = doc.createElement("Numero_de_Control");
		controlnumber.appendChild(doc.createTextNode(o.getControlNumber()));
		element.appendChild(controlnumber);
                
                Element firstname = doc.createElement("Nombre");
		firstname.appendChild(doc.createTextNode(o.getName()));
		element.appendChild(firstname);
                
                Element surname = doc.createElement("Apellido_materno");
		surname.appendChild(doc.createTextNode(o.getSurname()));
		element.appendChild(surname);
                
                Element lastname = doc.createElement("Apellido_paterno");
		lastname.appendChild(doc.createTextNode(o.getLastName()));
		element.appendChild(lastname);
                
                Element age = doc.createElement("Edad");
		age.appendChild(doc.createTextNode(o.getAge()));
		element.appendChild(age);
                
                Element sex = doc.createElement("Sexo");
		sex.appendChild(doc.createTextNode(o.getSex()));
		element.appendChild(sex);
                
                Element career = doc.createElement("Carrera");
		career.appendChild(doc.createTextNode(o.getCareer()));
		element.appendChild(career);
                
                Element semester = doc.createElement("Semestre");
		semester.appendChild(doc.createTextNode(o.getSemester()));
		element.appendChild(semester);
                c++;
            }
            
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("src\\Data\\data.xml"));
            transformer.transform(source, result);

            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Xml.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(Xml.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
